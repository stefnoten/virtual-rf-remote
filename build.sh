#!/usr/bin/env bash
set -e
IMAGE=stefnoten/virtual-rf-remote:0.0.1
docker build \
  --build-arg COMMIT=$(git rev-parse --short HEAD) \
  --platform linux/arm/v7 \
  --tag $IMAGE \
  .
docker push $IMAGE
