# Virtual RF remote

This program allows a Raspberry Pi to control RF devices using RF transmitters connected on GPIO pins.

Supported protocols:

* Somfy RTS (via a 433.42MHz transmitter)
* Security+ (via a 433.92MHz transmitter)

Configured devices are registered as Home Assistant devices over MQTT. Signals are sent to the RF transmitters over a running
[pigpiod](https://abyz.me.uk/rpi/pigpio/pigpiod.html) service.

## Hardware

Transmitters for 433MHz, e.g. [this one](https://www.amazon.nl/kwmobile-zender-ontvanger-radio-module/dp/B01H2D2RH6), typically come with a 433.42MHz SAW
resonator crystal. To use the transmitter for 433.92MHz, the resonator should be replaced with one tuned to that frequency.

An antenna significantly increases the range. Use an antenna such as [this one](https://www.amazon.nl/gp/product/B07QGQ1VG5) or a 17cm copper wire.

## Configuration

Configuration must be specified through a yaml file. The format and default values can be found in [Config.ts](./src/Config.ts), or in the
[example config file](./docker/configuration.yaml). The path to the configuration file must be specified in the `CONFIG` environment variable (preconfigured
when running with docker, see below).

The current rolling codes for the devices are stored in the file configured by the `state_file` property. This file should not be lost as the RF devices will
reject communication when rolling codes are out of range.

## Deployment

This package can be deployed using the [stefnoten/virtual-rf-remote](https://hub.docker.com/r/stefnoten/virtual-rf-remote) docker image.

Example docker-compose.yaml, with a configuration file at `./config/configuration.yaml`:

```yaml
version: '3'
services:
  pigpiod:
    image: zinen2/alpine-pigpiod:arm32v7
    ports:
      - "8888:8888"
    devices:
      - /dev/gpiochip0
    restart: unless-stopped
    privileged: true
  virtual-rf-remote:
    image: stefnoten/virtual-rf-remote:0.0.1
    volumes:
      - ./config:/app/data
      - /etc/localtime:/etc/localtime:ro
    restart: unless-stopped
```

# Pairing devices

## Somfy RTS

1. Make sure MQTT, pigpiod and Home Assistant are running
2. Connect a 433.42MHz transmitter to an available GPIO pin
3. Configure the used pin and the covers in the configuration file and (re)start this program
4. On an already linked remote, press and hold the `Prog` button until the cover briefly moves up and down
    * For a cover group, repeat for all configured covers
5. In Home Assistant, find the `Virtual RF Remote` device and switch on the `_program` switch for the configured cover (group). The cover(s) will acknowledge by briefly moving up and down again
