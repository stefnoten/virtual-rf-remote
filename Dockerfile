FROM node:14-alpine as node_modules
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm ci --production --no-audit --no-optional --no-update-notifier

FROM node:14-alpine as compilation
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm ci --no-audit --no-optional --no-update-notifier
COPY tsconfig.json index.ts ./
COPY src ./src
RUN npm run build

FROM node:14-alpine
RUN apk add --no-cache tzdata tini
WORKDIR /app
COPY package.json ./
COPY --from=node_modules /app/node_modules ./node_modules
COPY --from=compilation /app/dist ./dist
ARG COMMIT
RUN echo "commit=${COMMIT}" > .npmrc
ENV CONFIG=/app/data/configuration.yaml
CMD [ "/sbin/tini", "--", "npm", "run", "start:prod"]
