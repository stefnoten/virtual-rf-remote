import {config} from './src/Config';
import {MqttConnection} from './src/MqttConnection';
import {loadGpio, loadPhysicalDevices} from './src/physical';
import {usePersistentState, useWithDefault} from './src/state';
import {loadVirtualDevices} from './src/virtual';

const version = process.env.npm_config_commit;

startServer().catch(error => {
  console.error(error);
  process.exit(1);
});

async function startServer() {
  console.log(`Running version ${version}`);
  const client = new MqttConnection(config().mqtt);
  const devices = await loadDevices(client);
  console.log('Connecting to MQTT...');
  await client.connect();
  console.log('Registering devices with HomeAssistant...');
  await devices.registerDevices();
  await devices.listenForCommands();
}

function loadDevices(mqtt: MqttConnection) {
  const {physical_devices, virtual_devices, state_file, pigpiod} = config();
  const physicalDevices = loadPhysicalDevices(loadGpio(pigpiod), physical_devices);
  const state = useWithDefault(() => ({}), usePersistentState(state_file));
  return loadVirtualDevices({config: virtual_devices, physicalDevices, state, mqtt, version});
}
