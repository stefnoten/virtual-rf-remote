import * as fs from 'fs';
import {load} from 'js-yaml';
import objectAssignDeep from 'object-assign-deep';
import * as path from 'path';
import {MqttConfig} from './MqttConnection';
import {GpioConfig, PhysicalDevicesConfig} from './physical';
import {VirtualDevicesConfig} from './virtual';

export interface Config {
  mqtt: MqttConfig;
  pigpiod: GpioConfig;
  physical_devices: PhysicalDevicesConfig;
  virtual_devices: VirtualDevicesConfig;
  state_file: string;
}

const filename = process.env.CONFIG;
const defaults: DeepPartial<Config> = {
  mqtt: {
    base_topic: 'virtual-rf-remote',
    keepalive: 60
  },
  pigpiod: {},
  physical_devices: {},
  virtual_devices: {},
  state_file: path.join(filename, '../state.json')
}
let cachedConfig: Config;

export function config(): Config {
  if (!cachedConfig) {
    const values = load(fs.readFileSync(filename, 'utf8'), {filename});
    cachedConfig = objectAssignDeep(defaults, values) as Config;
  }
  return cachedConfig;
}

type DeepPartial<T> = {
  [P in keyof T]?: DeepPartial<T[P]>;
};
