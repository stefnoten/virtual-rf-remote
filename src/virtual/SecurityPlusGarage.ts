import {MqttConnection, MqttMessage} from '../MqttConnection';
import {DEVICE_433_92_MHz} from '../physical';
import {SecurityPlusDevice, SecurityPlusDeviceState} from './SecurityPlusDevice';
import {VirtualDevice, VirtualDeviceParams} from './VirtualDevice';

enum Command {
  OPEN = 'open',
  CLOSE = 'close',
  STOP = 'stop',
  PROGRAM = 'program',
  END_PROGRAM = 'end_program'
}

enum ButtonState {
  ON = 'on',
  OFF = 'off',
}

/**
 * A Security+ garage, identified by a 7-nibble address (e.g. 0x1234567)
 */
export interface SecurityPlusGarageConfig {
  type: 'security_plus_garage'
  friendly_name: string;
}

export function loadSecurityPlusGarage({id, state, physicalDevices, config, mqtt}: VirtualDeviceParams<SecurityPlusGarageConfig, SecurityPlusDeviceState>) {
  const device = new SecurityPlusDevice(+id, state, physicalDevices.get(DEVICE_433_92_MHz));
  return new SecurityPlusGarage(id, device, config.friendly_name, mqtt);
}

export class SecurityPlusGarage implements VirtualDevice {
  constructor(readonly id: string,
              private device: SecurityPlusDevice,
              readonly friendlyName: string,
              private mqtt: MqttConnection) {
  }

  private get programTopic() {
    return `${this.friendlyName}/program`;
  }

  get birthMessages(): MqttMessage[] {
    return [];
  }

  get discoveryMessages(): MqttMessage<object>[] {
    const {baseTopic, discoveryTopic} = this.mqtt;
    const {id, friendlyName, programTopic} = this;
    const uniqueId = `security_plus_garage_${id}`;
    const programId = `${uniqueId}_program`;
    return [
      {
        topic: `${discoveryTopic}/cover/${uniqueId}/config`,
        payload: {
          command_topic: `${baseTopic}/${friendlyName}/set`,
          device_class: 'garage',
          name: friendlyName,
          unique_id: uniqueId,
          payload_open: Command.OPEN,
          payload_close: Command.CLOSE,
          payload_stop: Command.STOP,
        }
      },
      {
        topic: `${discoveryTopic}/switch/${programId}/config`,
        payload: {
          command_topic: `${baseTopic}/${friendlyName}/set`,
          icon: 'mdi:wrench',
          name: `${friendlyName}_program`,
          unique_id: programId,
          payload_on: Command.PROGRAM,
          payload_off: Command.END_PROGRAM,
          state_on: ButtonState.ON,
          state_off: ButtonState.OFF,
          state_topic: `${baseTopic}/${programTopic}`,
        }
      }
    ];
  }

  async handleMessage(topicSegments: string[], payload: Buffer) {
    const [command] = topicSegments;
    switch (command) {
      case 'set':
        await this.handle(payload.toString());
        break;
    }
  }

  private async handle(command: string) {
    switch (command) {
      case Command.OPEN:
      case Command.CLOSE:
      case Command.STOP:
        this.log(`Press`);
        await this.device.pressOnce();
        break;
      case Command.PROGRAM:
        this.log(`Pressing program...`);
        await this.mqtt.publish({
          childTopic: this.programTopic,
          payload: ButtonState.ON
        });
        await this.device.pressHold();
        break;
      case Command.END_PROGRAM:
        await this.device.release();
        await this.mqtt.publish({
          childTopic: this.programTopic,
          payload: ButtonState.OFF
        });
        this.log(`Released program`);
        break;
      default:
        throw new Error(`${this}: Got an unknown command: "${command}"`);
    }
  }

  private log(...parts: string[]) {
    console.log(`[${this}]`, ...parts);
  }

  toString() {
    return `${this.constructor.name} ${this.friendlyName}`;
  }
}
