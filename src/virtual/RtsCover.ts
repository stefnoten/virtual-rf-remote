import {MqttConnection, MqttMessage} from '../MqttConnection';
import {DEVICE_433_42_MHz} from '../physical';
import {failWith, noop} from '../utils';
import {RtsDevice, RtsDeviceState} from './RtsDevice';
import {DiscoveryMessage, VirtualDevice, VirtualDeviceParams} from './VirtualDevice';

enum Command {
  OPEN = 'open',
  CLOSE = 'close',
  STOP = 'stop',
  PROGRAM = 'program',
  CLEAR = 'clear',
}

enum ButtonState {
  ON = 'on',
  OFF = 'off',
}

enum CoverState {
  OPEN = 'open',
  CLOSED = 'closed',
  STOPPED = 'stopped',
}

/**
 * An RTS cover, identified by a 3-byte hexadecimal address (e.g. 0x123456)
 */
export interface RtsCoverConfig {
  type: 'rts_cover'
  friendly_name: string;
  device_class?: string;
}

export function loadRtsCover({id, state, physicalDevices, config, mqtt}: VirtualDeviceParams<RtsCoverConfig, RtsDeviceState>) {
  const device = new RtsDevice(+id, state, physicalDevices.get(DEVICE_433_42_MHz));
  return new RtsCover(id, device, config.friendly_name, config.device_class, mqtt);
}

export class RtsCover implements VirtualDevice {
  private commands = {
    [Command.OPEN]: this.logged('Opening', () => this.open()),
    [Command.CLOSE]: this.logged('Closing', () => this.close()),
    [Command.STOP]: this.logged('Stop', () => this.stop()),
    [Command.PROGRAM]: this.logged('Pressing program', () => this.pressProgram(), 'Released program'),
    [Command.CLEAR]: noop,
  }
  private changeHandlers: ((state: CoverState) => Promise<void>)[] = [];

  constructor(readonly id: string,
              private device: RtsDevice,
              readonly friendlyName: string,
              private deviceClass: string = 'curtain',
              private mqtt: MqttConnection) {
  }

  private get stateTopic() {
    return `${this.friendlyName}`;
  }

  private get programTopic() {
    return `${this.friendlyName}/program`;
  }

  get homeAssistantId() {
    return `rts_cover_${this.id}`;
  }

  get optimistic() {
    return false;
  }

  get birthMessages(): MqttMessage[] {
    return [
      {
        childTopic: this.programTopic,
        payload: ButtonState.OFF,
      }
    ];
  }

  get discoveryMessages(): DiscoveryMessage[] {
    const {baseTopic, discoveryTopic} = this.mqtt;
    const {homeAssistantId, friendlyName, deviceClass, stateTopic, programTopic, optimistic} = this;
    const programId = `${homeAssistantId}_program`;
    return [
      {
        topic: `${discoveryTopic}/cover/${homeAssistantId}/config`,
        payload: {
          device_class: deviceClass,
          name: friendlyName,
          unique_id: homeAssistantId,
          command_topic: `${baseTopic}/${stateTopic}/set`,
          payload_open: Command.OPEN,
          payload_close: Command.CLOSE,
          payload_stop: Command.STOP,
          state_topic: `${baseTopic}/${stateTopic}`,
          state_open: CoverState.OPEN,
          state_closed: CoverState.CLOSED,
          state_stopped: CoverState.STOPPED,
          optimistic,
        }
      },
      {
        topic: `${discoveryTopic}/switch/${programId}/config`,
        payload: {
          icon: 'mdi:wrench',
          name: `${friendlyName}_program`,
          unique_id: programId,
          command_topic: `${baseTopic}/${stateTopic}/set`,
          payload_on: Command.PROGRAM,
          payload_off: Command.CLEAR,
          state_topic: `${baseTopic}/${programTopic}`,
          state_on: ButtonState.ON,
          state_off: ButtonState.OFF,
        }
      }
    ];
  }

  async handleMessage(topicSegments: string[], payload: Buffer) {
    const [command] = topicSegments;
    switch (command) {
      case 'set':
        const action = this.commands[payload.toString()] ?? failWith(`${this}: Received an unknown command: "${command}"`);
        await action();
        break;
    }
  }

  private async open() {
    await this.markOpen();
    await this.device.open();
  }

  private async close() {
    await this.markClosed();
    await this.device.close();
  }

  async markOpen() {
    await this.emitState(CoverState.OPEN);
  }

  async markClosed() {
    await this.emitState(CoverState.CLOSED);
  }

  async markStopped() {
    await this.emitState(CoverState.STOPPED);
  }

  onChange(fn: (state: CoverState) => Promise<void>) {
    this.changeHandlers.push(fn);
  }

  private async stop() {
    await this.markStopped();
    await this.device.stop();
  }

  private async pressProgram() {
    await this.emitProgramState(ButtonState.ON);
    await this.device.program();
    await this.emitProgramState(ButtonState.OFF);
  }

  private async emitState(state: CoverState) {
    await Promise.allSettled([
      this.optimistic
        ? Promise.resolve()
        : this.mqtt.publish({childTopic: this.stateTopic, payload: state}),
      ...this.changeHandlers.map(fn => fn(state))
    ]);
  }

  private emitProgramState(state: ButtonState) {
    return this.mqtt.publish({
      childTopic: this.programTopic,
      payload: state
    });
  }

  private logged(...steps: (string | (() => void | Promise<void>))[]) {
    return async () => {
      for (let step of steps) {
        typeof step === 'string'
          ? console.log(`[${this}] ${step}`)
          : await step();
      }
    };
  }

  toString() {
    return `${this.constructor.name} ${this.friendlyName}`;
  }
}
