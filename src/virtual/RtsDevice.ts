import {GpioDevice} from '../physical';
import {Button, rtsFrames, RtsMessage, rtsPayload} from '../physical/protocol/rts';
import {LiveState, useSlice, useWithDefault} from '../state';

export interface RtsDeviceState {
  next_rolling_code?: number;
}

export class RtsDevice {
  private nextRollingCode = useWithDefault(() => 0, useSlice(this.state, 'next_rolling_code'));

  constructor(private senderId: number,
              private state: LiveState<RtsDeviceState>,
              private gpio: GpioDevice) {
  }

  async open() {
    await this.send(Button.UP);
  }

  async close() {
    await this.send(Button.DOWN);
  }

  async stop() {
    await this.send(Button.MY);
  }

  async program() {
    await this.send(Button.PROGRAM);
  }

  private async send(button: Button, repeats = 4) {
    const message = this.nextMessageFor(button);
    this.log(message);
    const wave = rtsFrames(rtsPayload(message), repeats);
    await this.gpio.send(wave);
  }

  private nextMessageFor(button: Button) {
    return {
      senderId: this.senderId,
      button,
      rollingCode: this.nextRollingCode.value++
    };
  }

  private log(message: RtsMessage) {
    const button = message.button.toString(2).padStart(4, '0');
    const rollingCode = message.rollingCode.toString().padStart(5, ' ');
    const senderId = message.senderId.toString(16).padStart(6, '0');
    console.log(`  [0x${senderId} @ ${rollingCode}] Button ${button}`);
  }
}
