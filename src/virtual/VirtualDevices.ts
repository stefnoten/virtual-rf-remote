import {Availability, MqttConnection, MqttMessage} from '../MqttConnection';
import {PhysicalDevices} from '../physical';
import {LiveState, useSlice, useWithDefault} from '../state';
import {failWith} from '../utils';
import {loadRtsCover} from './RtsCover';
import {loadRtsCoverGroup} from './RtsCoverGroup';
import {loadSecurityPlusGarage} from './SecurityPlusGarage';
import {VirtualDevice, VirtualDeviceConfig, VirtualDeviceParams} from './VirtualDevice';

export interface VirtualDevicesConfig {
  [id: string]: VirtualDeviceConfig;
}

export async function loadVirtualDevices({config, mqtt, physicalDevices, state, version}: {
  config: VirtualDevicesConfig;
  state: LiveState<Record<string, any>>;
  mqtt: MqttConnection;
  physicalDevices: PhysicalDevices;
  version: string;
}) {
  let resolve: <R extends VirtualDevice>(id: string) => R = () => failWith('Devices have not yet been initialized');
  const devices = Object.entries(config).map(([id, deviceConfig]) =>
    loadDevice({
      id,
      mqtt,
      physicalDevices,
      config: deviceConfig,
      state: useWithDefault(() => ({}), useSlice(state, id)),
      resolveDevice: id => resolve(id)
    }));
  resolve = <R extends VirtualDevice>(id) => devices.find(d => d.id === id) as unknown as R
  return VirtualDevices.initialize(mqtt, devices, version);
}

function loadDevice(params: VirtualDeviceParams) {
  const {config} = params;
  const {type} = config;
  switch (type) {
    case 'rts_cover':
      return loadRtsCover({...params, config});
    case 'rts_cover_group':
      return loadRtsCoverGroup({...params, config});
    case 'security_plus_garage':
      return loadSecurityPlusGarage({...params, config});
    default:
      throw new Error(`Unknown device type: ${type}`);
  }
}

export class VirtualDevices {
  private constructor(private mqtt: MqttConnection, private devices: VirtualDevice[], private version: string) {
  }

  static async initialize(mqtt: MqttConnection, devices: VirtualDevice[], version: string) {
    await Promise.allSettled(devices.map(device => device.initialize?.()));
    return new VirtualDevices(mqtt, devices, version);
  }

  private get deviceDiscoveryInfo() {
    return {
      availability: [
        {
          payload_available: Availability.ONLINE,
          payload_not_available: Availability.OFFLINE,
          topic: this.mqtt.availabilityTopic,
        }
      ],
      device: {
        identifiers: ['virtual_rf_remote'],
        manufacturer: 'Stef Noten',
        model: 'Virtual RF Remote',
        name: 'Virtual RF Remote',
        sw_version: `Virtual RF Remote ${this.version}`
      }
    };
  }

  async registerDevices() {
    return Promise.allSettled(
      this.devices.map(device => this.registerDevice(device))
    );
  }

  async registerDevice(device: VirtualDevice) {
    console.log(`  Registering device: ${device}`);
    await this.mqtt.publishAll(device.discoveryMessages.map(m => this.withDeviceInfo(m)));
    await this.mqtt.publishAll(device.birthMessages);
  }

  device(friendlyName: string) {
    return this.devices.find(d => d.friendlyName === friendlyName);
  }

  async listenForCommands() {
    await this.mqtt.subscribe({childTopic: '#'});
    this.mqtt.onMessage(([friendlyName, ...segments], payload) => {
      this.device(friendlyName)?.handleMessage(segments, payload);
    });
  }

  private withDeviceInfo(message: MqttMessage<object>) {
    return {
      ...message,
      payload: {
        ...message.payload,
        ...this.deviceDiscoveryInfo,
      }
    };
  }
}
