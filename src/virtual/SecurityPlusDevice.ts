import {GpioDevice} from '../physical';
import {securityPlusWave} from '../physical/protocol/securityplus';
import {LiveState, useSlice, useWithDefault} from '../state';
import {composeCleanup, noop} from '../utils';

export interface SecurityPlusDeviceState {
  next_rolling_code?: number;
}

export class SecurityPlusDevice {
  private nextRollingCode = useWithDefault(() => 0, useSlice(this.state, 'next_rolling_code'));
  private cancelCurrentWave = noop;

  constructor(private senderId: number,
              readonly state: LiveState<SecurityPlusDeviceState>,
              private gpio: GpioDevice) {
  }

  async pressOnce() {
    const message = this.nextMessage();
    this.log('Pressed', message);
    await this.gpio.send(securityPlusWave(this.nextMessage(), 4));
  }

  async pressHold() {
    const message = this.nextMessage();
    this.log('Pressing...', message);
    const cancel = this.gpio.sendRepeated(securityPlusWave(message, 1));
    this.cancelCurrentWave = composeCleanup(this.cancelCurrentWave, cancel);
  }

  async release() {
    this.log('Released');
    this.cancelCurrentWave();
    this.cancelCurrentWave = noop;
  }

  private nextMessage() {
    return {
      senderId: this.senderId,
      rollingCode: this.nextRollingCode.value += 2
    };
  }

  private log(action: string, {senderId = this.senderId, rollingCode}: { senderId?: number, rollingCode?: number } = {}) {
    const rollingCodePart = rollingCode === undefined
      ? ''
      : ` @ ${rollingCode.toString().padStart(5, ' ')}`;
    const hexSenderId = senderId.toString(16).padStart(6, '0');
    console.log(`  [0x${hexSenderId}${rollingCodePart}] ${action}`);
  }
}
