import * as Buffer from 'buffer';
import {MqttConnection, MqttMessage} from '../MqttConnection';
import {PhysicalDevices} from '../physical';
import {LiveState} from '../state';
import {RtsCoverConfig} from './RtsCover';
import {RtsCoverGroupConfig} from './RtsCoverGroup';
import {SecurityPlusGarageConfig} from './SecurityPlusGarage';

export type VirtualDeviceConfig =
  | RtsCoverConfig
  | RtsCoverGroupConfig
  | SecurityPlusGarageConfig;

export type DiscoveryMessage = MqttMessage & { payload: object; };

export interface VirtualDeviceParams<CONFIG extends VirtualDeviceConfig = VirtualDeviceConfig, STATE = any> {
  id: string;
  config: CONFIG;
  state: LiveState<STATE>;
  mqtt: MqttConnection;
  physicalDevices: PhysicalDevices;
  resolveDevice: <R extends VirtualDevice>(id: string) => R;
}

export interface VirtualDevice {
  readonly id: string;

  readonly friendlyName: string;

  readonly birthMessages: MqttMessage[];

  readonly discoveryMessages: MqttMessage<object>[];

  handleMessage(topicSegments: string[], payload: Buffer): Promise<void>;

  initialize?(): Promise<void>;
}
