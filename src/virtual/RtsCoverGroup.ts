import {MqttConnection} from '../MqttConnection';
import {DEVICE_433_42_MHz} from '../physical';
import {RtsCover} from './RtsCover';
import {RtsDevice, RtsDeviceState} from './RtsDevice';
import {VirtualDeviceParams} from './VirtualDevice';

/**
 * An RTS cover group, identified by a 3-byte hexadecimal address (e.g. 0x123456)
 */
export interface RtsCoverGroupConfig {
  type: 'rts_cover_group'
  friendly_name: string;
  device_class?: string;
  cover_ids: string[];
}

export function loadRtsCoverGroup({id, state, physicalDevices, config, mqtt, resolveDevice}: VirtualDeviceParams<RtsCoverGroupConfig, RtsDeviceState>) {
  const device = new RtsDevice(+id, state, physicalDevices.get(DEVICE_433_42_MHz));
  const {friendly_name, device_class, cover_ids} = config;
  const resolveCovers = () => cover_ids.map<RtsCover>(resolveDevice);
  return new RtsCoverGroup(id, device, resolveCovers, friendly_name, device_class, mqtt);
}

export class RtsCoverGroup extends RtsCover {

  constructor(id: string,
              device: RtsDevice,
              private resolveCovers: () => RtsCover[],
              friendlyName: string,
              deviceClass: string,
              mqtt: MqttConnection) {
    super(id, device, friendlyName, deviceClass, mqtt);
  }

  get homeAssistantId(): string {
    return `rts_cover_group_${this.id}`;
  }

  get optimistic(): boolean {
    return true;
  }

  async initialize() {
    this.resolveCovers().forEach(cover => cover.onChange(() => this.markStopped()));
  }

  async markOpen() {
    await super.markOpen();
    await this.eachCover(c => c.markOpen());
  }

  private async eachCover(action: (cover: RtsCover) => Promise<void>) {
    await Promise.all(this.resolveCovers().map(action));
  }

  async markClosed() {
    await super.markClosed();
    await this.eachCover(c => c.markClosed());
  }
}
