import {useSlice} from './useSlice';

test('origin updates are reflected', () => {
  const origin = {
    value: {counter: 0}
  };
  const counter = useSlice(origin, 'counter');
  origin.value = {counter: 1};
  expect(counter.value).toEqual(1);
});

test('updates are forwarded to the origin value', () => {
  const origin = {
    value: {counter: 0}
  };
  const counter = useSlice(origin, 'counter');
  counter.value++;
  expect(origin.value.counter).toEqual(1);
});
