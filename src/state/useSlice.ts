import {LiveState} from './LiveState';

export function useSlice<T extends object, K extends keyof T>(origin: LiveState<T>, key: K): LiveState<T[K]> {
  return {
    get value() {
      return origin.value[key];
    },
    set value(value: T[K]) {
      origin.value = Object.assign(origin.value, ({[key]: value}));
    }
  };
}
