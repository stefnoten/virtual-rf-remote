import fs from 'fs';
import {LiveState} from './LiveState';
import {useCached} from './useCached';

export function usePersistentState<T extends object>(filename: string): LiveState<T> {
  return useCached({
    get value() {
      return fs.existsSync(filename)
        ? JSON.parse(fs.readFileSync(filename).toString()) as T
        : undefined;
    },
    set value(value: T) {
      fs.writeFileSync(filename, JSON.stringify(value, null, 2));
    }
  });
}
