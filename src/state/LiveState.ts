export interface LiveState<T> {
  value: T;
}
