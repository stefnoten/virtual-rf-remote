import {LiveState} from './LiveState';

export function useDerivedState<ORIGIN, T>({origin, map, mapBack}: {
  origin: LiveState<ORIGIN>,
  map: (myValue: ORIGIN) => T,
  mapBack: (myPrevious: ORIGIN, newValue: T) => ORIGIN
}): LiveState<T> {
  return {
    get value(): T {
      return map(origin.value);
    },
    set value(value: T) {
      origin.value = mapBack(origin.value, value);
    }
  };
}
