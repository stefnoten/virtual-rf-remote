import {LiveState} from './LiveState';

export function useWithDefault<T>(defaultValue: () => T, state: LiveState<T | undefined>): LiveState<T> {
  return {
    get value() {
      return state.value ?? (state.value = defaultValue());
    },
    set value(value: T) {
      state.value = value;
    }
  };
}
