import {LiveState} from './LiveState';
import {useWithDefault} from './useWithDefault';

test('returns values from underlying state', () => {
  const origin = {value: 1};
  const result = useWithDefault(() => 0, origin);
  expect(result.value).toEqual(1);
  origin.value++;
  expect(result.value).toEqual(2);
});

test('returns default if value is undefined', () => {
  const origin = {} as LiveState<number>;
  const result = useWithDefault(() => 0, origin);
  expect(result.value).toEqual(0);
});

test('does not reset after default was assigned', () => {
  const origin = {} as LiveState<number>;
  const result = useWithDefault(() => 0, origin);
  result.value++;
  expect(result.value).toEqual(1);
});

test('returns default if value becomes undefined', () => {
  const origin = {value: 1};
  const result = useWithDefault(() => 0, origin);
  delete origin.value;
  expect(result.value).toEqual(0);
});
