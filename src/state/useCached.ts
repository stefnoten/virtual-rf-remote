import {LiveState} from './LiveState';

export function useCached<T>(origin: LiveState<T>): LiveState<T> {
  let current = origin.value;
  return {
    get value() {
      return current;
    },
    set value(value: T) {
      current = value;
      origin.value = value;
    }
  };
}
