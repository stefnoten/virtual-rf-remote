import {OUTPUT} from '@node-pigpio/core';
import {Cleanup, deferred, failWith, taskQueue, until} from '../utils';
import {Gpio} from './Gpio';
import {GpioDevice, GpioDeviceConfig} from './GpioDevice';
import {toPulses, Wave} from './wave';

export const DEVICE_433_42_MHz = '433.42MHz';
export const DEVICE_433_92_MHz = '433.92MHz';
export type SupportedDeviceNames = typeof DEVICE_433_42_MHz | typeof DEVICE_433_92_MHz;

export type PhysicalDevicesConfig = Record<SupportedDeviceNames, GpioDeviceConfig>;

export function loadPhysicalDevices(gpio: Gpio, config: PhysicalDevicesConfig) {
  return new PhysicalDevices(gpio, config);
}

/**
 * https://github.com/joan2937/pigpio/issues/331
 */
const PIGPIO_DELAY_MULTIPLIER = 0.5;

export class PhysicalDevices {
  private queue = taskQueue();

  constructor(private gpio: Gpio, private devices: PhysicalDevicesConfig) {
  }

  get(name: SupportedDeviceNames): GpioDevice {
    const config = this.devices[name] ?? failWith(`Unknown device: ${name}`);
    return {
      send: wave => this.send(wave, config.gpio_pin),
      sendRepeated: wave => this.sendRepeated(wave, config.gpio_pin),
    };
  }

  private async send(wave: Wave, pin: number) {
    await this.queue.next(async () => {
      const id = await this.createWave(wave, pin);
      await this.gpio.set_mode(pin, OUTPUT)
      await this.gpio.wave_send_once(id);
      await until(async () => !await this.gpio.wave_tx_busy());
      await this.gpio.wave_delete(id);
    });
  }

  private sendRepeated(wave: Wave, pin: number): Cleanup {
    const cancellation = deferred<void>();
    this.queue.next(async () => {
      const id = await this.createWave(wave, pin);
      await this.gpio.set_mode(pin, OUTPUT)
      await this.gpio.wave_send_repeat(id);
      await cancellation;
      await this.gpio.wave_tx_stop();
      await this.gpio.wave_delete(id);
    });
    return () => void cancellation.resolve();
  }

  private async createWave(wave: Wave, pin: number) {
    await this.gpio.wave_clear();
    await this.gpio.wave_add_generic(toPulses(pin, wave).map(pulse => ({...pulse, delay: pulse.delay * PIGPIO_DELAY_MULTIPLIER})));
    const id = await this.gpio.wave_create();
    if (id < 0) {
      throw new Error(`Could not create wave, got code ${id}`);
    }
    return id;
  }
}
