import {bits, BYTE, bytes, NIBBLE, Signal} from '../../signal';
import {manchester, off, on, sequence, Wave} from '../../wave';
import {RtsMessage} from './RtsMessage';

export function rtsPayload({button, rollingCode, senderId}: RtsMessage): Signal {
  const data = [
    0xA2,
    button << 4,
    ...BYTE.digitsOfBE(rollingCode, {count: 2}),
    ...BYTE.digitsOfBE(senderId, {count: 3}),
  ];
  data[1] |= checksum(data);
  return obfuscate(data);
}

function checksum(data: number[]) {
  return bytes(data).as(NIBBLE).values.reduce((a, b) => a ^ b);
}

function obfuscate(data: number[]) {
  return bytes(data).scan((acc, value) => acc ^ value, 0);
}

export function rtsFrames(payload: Signal, times: number): Wave {
  return Array(times).fill(undefined)
    .flatMap((v, i) => rtsFrame(payload, {isRepeat: i !== 0}));
}

export function rtsFrame(payload: Signal, {isRepeat = false} = {}): Wave {
  return [
    isRepeat
      ? sequence(bits('10'.repeat(7)), {usPerBit: 2560})
      : [
        on(9415),
        off(89565),
        sequence(bits('10'.repeat(2)), {usPerBit: 2560}),
      ],
    on(4550),
    off(640),
    manchester(payload, {usPerBit: 1280}),
    off(30415)
  ];
}
