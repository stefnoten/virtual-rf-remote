export enum Button {
  MY = 0x1,
  UP = 0x2,
  DOWN = 0x4,
  PROGRAM = 0x8,
}

export interface RtsMessage {
  button: Button;
  rollingCode: number;
  senderId: number;
}
