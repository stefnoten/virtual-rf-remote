import {BIT, bytes} from '../../signal';
import {toPulses} from '../../wave';
import {rtsFrame, rtsPayload} from './rts';
import {Button} from './RtsMessage';

describe('rtsPayload', () => {
  test('returns correct value', () => {
    const payload = rtsPayload({button: Button.PROGRAM, rollingCode: 0xabcd, senderId: 0x123456});
    expect(payload).toEqual(bytes('a2258e43516533'));
  });
});

describe('rtsFrame', () => {
  const pin = 1 << 12;

  test('is correct', () => {
    const payload = rtsPayload({button: Button.PROGRAM, rollingCode: 19, senderId: 0xb032b3});
    console.log(payload.as(BIT).toString());
    const pulses = toPulses(12, rtsFrame(payload, {isRepeat: true}));
    console.log(
      pulses.map(asBit1).join('') + '\n' + pulses.map(asBit2).join('') + '\n\n' + pulses.map(asDelay).join(''));
  });

  // 10100010001001011000111001000011010100010110010100110011

  function asBit1(v: any) {
    const {gpio_on, gpio_off} = v;
    if ((gpio_on === 0 && gpio_off !== pin) || (gpio_off === 0 && gpio_on !== pin)) {
      throw new Error(`hmmmm, got ${gpio_on} (on) and ${gpio_off} (off)`);
    }
    const sign = gpio_on === 0 ? 0 : 1;
    return sign ? `|‾‾‾‾‾` : '|     ';
  }

  function asBit2(v: any) {
    const {gpio_on, gpio_off} = v;
    if ((gpio_on === 0 && gpio_off !== pin) || (gpio_off === 0 && gpio_on !== pin)) {
      throw new Error(`hmmmm, got ${gpio_on} (on) and ${gpio_off} (off)`);
    }
    const sign = gpio_on === 0 ? 0 : 1;
    return sign ? `|     ` : '|_____';
  }

  function asDelay(v: any) {
    const {delay} = v;
    const s = delay.toString();
    const length = 6;
    const pad = Math.floor(length - s.length) / 2;
    return (s + ' '.repeat(pad)).padStart(length, ' ');
  }
});
