import {BIT, bits, Signal} from '../../signal';
import {FRAME1_PREFIX, FRAME2_PREFIX, parseSecurityPlusFrames, securityPlusFrames} from './securityPlusFrames';

const messages = [
  {
    frame1: Signal.concat(FRAME1_PREFIX, bits('00010001011101110011001100010011001100110011001101110001001100010011000100010111')),
    frame2: Signal.concat(FRAME2_PREFIX, bits('01110001000100110011000100110111001100010111000101110111001100110111001100110011')),
    rollingCode: 187532012,
    fixedCode: 168968444
  },
  {
    frame1: Signal.concat(FRAME1_PREFIX, bits('00110011011100010001001100110111001101110011011100010111011100010111001100110011')),
    frame2: Signal.concat(FRAME2_PREFIX, bits('00110111011101110111011100010001001100110011000101110111000100010111000101110011')),
    rollingCode: 187532014,
    fixedCode: 168968444
  },
  {
    frame1: Signal.concat(FRAME1_PREFIX, bits('00010001011101110001000100010001001100010001011101110011000100010111001100110011')),
    frame2: Signal.concat(FRAME2_PREFIX, bits('01110001000100110011000101110001001100110011000101110111001100110111001101110111')),
    rollingCode: 187532020,
    fixedCode: 168968444
  },
  {
    frame1: Signal.concat(FRAME1_PREFIX, bits('00010001001100110011000100010001000101110011011100110001011100110111011100010011')),
    frame2: Signal.concat(FRAME2_PREFIX, bits('01110001000100110111001100110001000100010111000101110111011101110001000100110001')),
    rollingCode: 187532024,
    fixedCode: 168968444
  }
];

describe('Parse Security+ message', () => {
  messages.forEach(({frame1, frame2, rollingCode, fixedCode}, i) => {
    test(`Message ${i}`, () => {
      const actual = parseSecurityPlusFrames([frame1, frame2]);
      expect(actual.rollingCode).toBe(rollingCode);
      expect(actual.senderId).toBe(fixedCode);
    });
  });
});

describe('Generate Security+ message', () => {
  messages.forEach(({frame1, frame2, rollingCode, fixedCode}, i) => {
    test(`Message ${i}`, () => {
      const [actualFrame1, actualFrame2] = securityPlusFrames({rollingCode, senderId: fixedCode});
      expect(actualFrame1.as(BIT)).toEqual(frame1);
      expect(actualFrame2.as(BIT)).toEqual(frame2);
    });
  });
});

