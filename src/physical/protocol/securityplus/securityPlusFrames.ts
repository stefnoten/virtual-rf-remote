import {isEven, isOdd} from '../../../utils';
import {BIT, bits, NIBBLE, nibbles, Signal} from '../../signal';

export interface SecurityPlusMessage {
  rollingCode: number;
  senderId: number;
}

export const FRAME1_PREFIX = bits('0001').as(NIBBLE);
export const FRAME2_PREFIX = bits('0111').as(NIBBLE);
export const FRAME_BIT_LENGTH = securityPlusFrames({senderId: 0, rollingCode: 0})[0].as(BIT).length;

export function securityPlusFrames(message: SecurityPlusMessage): [Signal, Signal] {
  const {rollingCode, senderId} = message;
  const [rollingCodeFrame1, rollingCodeFrame2] = nibbles(tritsOfBE(reverseBits(rollingCode), {count: 20})).takePer(10);
  const [fixedCodeFrame1, fixedCodeFrame2] = nibbles(tritsOfBE(senderId, {count: 20})).takePer(10);
  return [
    Signal.concat(FRAME1_PREFIX, encodeRollingAndFixedCodeFrame(rollingCodeFrame1, fixedCodeFrame1)),
    Signal.concat(FRAME2_PREFIX, encodeRollingAndFixedCodeFrame(rollingCodeFrame2, fixedCodeFrame2)),
  ];
}

function encodeRollingAndFixedCodeFrame(rollingCodeFrame: Signal, fixedCodeFrame: Signal) {
  const obfuscatedFixedCodeFrame = obfuscateFixedCodeFrame(fixedCodeFrame, rollingCodeFrame);
  return Signal.zip([rollingCodeFrame, obfuscatedFixedCodeFrame], (a, b) => [a, b]).map(encodeTrit);
}

function obfuscateFixedCodeFrame(fixedCodeFrame: Signal, rollingCodeFrame: Signal) {
  return fixedCodeFrame.scan((prevObfuscated, value, i) => (value + prevObfuscated + rollingCodeFrame.values[i]) % 3, 0);
}

export function parseSecurityPlusFrames(frames: [Signal, Signal]): SecurityPlusMessage {
  const [frame1, frame2] = [
    frames[0].as(NIBBLE).dropPrefix(FRAME1_PREFIX).map(toTrit),
    frames[1].as(NIBBLE).dropPrefix(FRAME2_PREFIX).map(toTrit),
  ];
  const [rollingCode, obfuscatedFixedCode] = Signal.concat(frame1, frame2).takeAt([isEven, isOdd]);
  const shiftedFixedCode = Signal.concat(
    frame1.takeAt(isOdd).shiftRight(0),
    frame2.takeAt(isOdd).shiftRight(0),
  );
  const fixedCode = Signal.zip([obfuscatedFixedCode, shiftedFixedCode, rollingCode], (f, a, b) => mod(f - a - b, 3));
  const rollingCodeValue = reverseBits(valueOfTrits(rollingCode));
  const fixedCodeValue = valueOfTrits(fixedCode);
  return {
    rollingCode: rollingCodeValue,
    senderId: fixedCodeValue,
  };
}

function reverseBits(value: number) {
  return BIT.digitsOfBE(value, {count: 32})
    .reduceRight((result, value) => (result << 1) + value, 0) >>> 0;
}

function valueOfTrits(signal: Signal) {
  return signal.values.reduce((result, value) => (result * 3) + value, 0);
}

function toTrit(nibble: number) {
  switch (nibble) {
    case 0b0001:
      return 0;
    case 0b0011:
      return 1;
    case 0b0111:
      return 2;
    default:
      throw new Error(`Unexpected encoded trit: 0b${nibble.toString(2)}`)
  }
}

function encodeTrit(trit: number) {
  switch (trit) {
    case 0:
      return 0b0001;
    case 1:
      return 0b0011;
    case 2:
      return 0b0111;
    default:
      throw new Error(`Unexpected trit: ${trit}`)
  }
}

function tritsOfLE(value: number, {count}: { count: number }) {
  if (value >= 3 ** count) {
    throw new Error(`Value '${value}' cannot be represented by ${count} trits`);
  }
  return Array.from(Array(count), (v, i) => Math.floor(value / 3 ** i) % 3);
}

function tritsOfBE(value: number, {count}: { count: number }) {
  return tritsOfLE(value, {count}).reverse();
}

function mod(value: number, modulus: number) {
  return ((value % modulus) + modulus) % modulus;
}
