import {off, sequence, Wave} from '../../wave';
import {FRAME_BIT_LENGTH, securityPlusFrames, SecurityPlusMessage} from './securityPlusFrames';

const US_PER_BIT = 500;
const US_BETWEEN_FRAMES = 100e3 - FRAME_BIT_LENGTH * US_PER_BIT;

export function securityPlusWave(message: SecurityPlusMessage, repeats: number): Wave {
  const frames = securityPlusFrames(message);
  return [...Array(repeats)].map(() =>
    frames.map(frame => [
      sequence(frame, {usPerBit: US_PER_BIT}),
      off(US_BETWEEN_FRAMES)
    ])
  );
}
