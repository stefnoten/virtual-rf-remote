import {Pulse} from '@node-pigpio/core';
import {toPulses} from './toPulses';
import {Wave} from './Wave';

test('flattens nested waves', () => {
  // given
  const wave: Wave = [
    {on: true, usDelay: 1},
    {on: false, usDelay: 2},
    [
      {on: true, usDelay: 3},
      [
        {on: false, usDelay: 4},
        {on: true, usDelay: 5},
      ],
      [
        [
          {on: false, usDelay: 6},
        ]
      ]
    ],
    {on: true, usDelay: 7},
  ];

  // when
  const result = toPulses(10, wave);

  // then
  expect(result).toEqual([
    {gpio_on: 1 << 10, gpio_off: 0, delay: 1},
    {gpio_on: 0, gpio_off: 1 << 10, delay: 2},
    {gpio_on: 1 << 10, gpio_off: 0, delay: 3},
    {gpio_on: 0, gpio_off: 1 << 10, delay: 4},
    {gpio_on: 1 << 10, gpio_off: 0, delay: 5},
    {gpio_on: 0, gpio_off: 1 << 10, delay: 6},
    {gpio_on: 1 << 10, gpio_off: 0, delay: 7},
  ] as Pulse[]);
});
