import {Pulse} from '@node-pigpio/core';
import {Wave, WaveStep} from './Wave';

export function toPulses(pin: number, wave: Wave): Pulse[] {
  const steps = wave.flat(Infinity) as WaveStep[];
  return steps.map(({on, usDelay}) => ({
    gpio_on: on ? (1 << pin) : 0,
    gpio_off: on ? 0 : (1 << pin),
    delay: usDelay
  }));
}
