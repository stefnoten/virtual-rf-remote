import {BIT, Signal} from '../signal';

export interface WaveStep {
  on: boolean;
  usDelay: number;
}

export type Wave = (WaveStep | Wave)[];

export function on(usDelay: number): WaveStep {
  return {on: true, usDelay};
}

export function off(usDelay: number): WaveStep {
  return {on: false, usDelay};
}

export function sequence(signal: Signal, {usPerBit}: { usPerBit: number }): Wave {
  return signal.as(BIT).values.map(on => ({on: !!on, usDelay: usPerBit}));
}

export function manchester(signal: Signal, {usPerBit}: { usPerBit: number }): Wave {
  const physicalBits = signal.as(BIT).flatMap(bit => [1 - bit, bit]);
  return sequence(physicalBits, {usPerBit: usPerBit / 2});
}
