import {BYTE} from '../signal';

export type WaveId = number;
export type WaveChainCommands = number;

export class WaveChain {
  private commands: (WaveId | WaveChainCommands)[] = [];

  wave = (waveId: WaveId) => this.add(waveId);

  repeatWave = (waveId: WaveId, times: number) => this.markStart().wave(waveId).markRepeat(times);

  repeatWaveForever = (waveId: WaveId) => this.markStart().wave(waveId).markRepeatForever();

  end = () => this.commands;

  private markStart = () => this.add(255, 0);

  private markRepeat = (times: number) => this.add(255, 1, ...BYTE.digitsOfLE(times, {count: 2}));

  private markRepeatForever = () => this.add(255, 3);

  private add(...items: (WaveId | WaveChainCommands)[]) {
    this.commands.push(...items);
    return this;
  }
}

export function newWaveChain() {
  return new WaveChain();
}
