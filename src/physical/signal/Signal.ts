import {Predicate} from '../../utils';
import {BIT, BYTE, NIBBLE, Unit, WORD} from './Unit';

export class Signal {
  constructor(readonly values: number[], readonly unit: Unit) {
    values.forEach(v => unit.validate(v));
  }

  get length() {
    return this.values.length;
  }

  static of(values: number[] | string, unit: Unit) {
    const numbers = typeof values === 'string'
      ? [...values].map(v => parseInt(v, 16))
      : values;
    return new Signal(numbers, unit);
  }

  static concat(...signals: Signal[]) {
    const unit = Unit.smallest(signals.map(s => s.unit));
    const values = signals.flatMap(s => s.as(unit).values);
    return new Signal(values, unit);
  }

  static zip(signals: Signal[], map: (...values: number[]) => number | number[]) {
    if (signals.length === 0) {
      throw new Error('At least one signal is needed');
    }
    assertSameShape(signals);
    return new Signal(signals[0].values.flatMap((v, i) => map(...signals.map(s => s.values[i]))), signals[0].unit);
  }

  map(fn: (value: number, index: number, values: number[]) => number): Signal {
    return new Signal(this.values.map(fn), this.unit);
  }

  flatMap(fn: (value: number) => number[]): Signal {
    return new Signal(this.values.flatMap(fn), this.unit);
  }

  scan(fn: (accumulator: number, current: number, index: number) => number, seed: number) {
    const newValues = Array(this.length);
    this.values.reduce(
      (accumulator, current, i) => newValues[i] = fn(accumulator, current, i),
      seed
    )
    return new Signal(newValues, this.unit);
  }

  startsWith(other: Signal) {
    assertSameUnit([this, other]);
    return this.length >= other.length &&
      other.values.every((value, i) => this.values[i] === value);
  }

  dropPrefix(prefix: Signal) {
    if (!this.startsWith(prefix)) {
      throw new Error(`This signal does not start with ${prefix}`);
    }
    return this.slice(prefix.length);
  }

  shiftRight(fill = 0) {
    return new Signal([fill, ...this.values.slice(0, -1)], this.unit);
  }

  takeAt(indexTest: Predicate<number>): Signal;
  takeAt(indexTests: Predicate<number>[]): Signal[];
  takeAt(indexTestOrTests: Predicate<number> | Predicate<number>[]) {
    return Array.isArray(indexTestOrTests)
      ? indexTestOrTests.map(test => this.takeAt(test))
      : new Signal(this.values.filter((v, i) => indexTestOrTests(i)), this.unit);
  }

  takePer(length: number) {
    if (this.length % length !== 0) {
      throw new Error(`Signal of length ${this.length} can't be split in chunks of length ${length}`);
    }
    return Array.from(Array(this.length / length), (v, i) => this.slice(i * length, (i + 1) * length));
  }

  slice(start?: number, end?: number) {
    return new Signal(this.values.slice(start, end), this.unit);
  }

  toString() {
    const separator = this.unit.hexDigits === 1 ? '' : ' ';
    return this.values.map(v => this.unit.toHex(v)).join(separator);
  }

  as(unit: Unit) {
    if (unit == this.unit) {
      return this;
    }
    if (unit > this.unit) {
      const reinterpretedValues = chunks(this.values, this.unit.digitsPer(unit)).map(chunk => valueOf(chunk, this.unit.bitsPerDigit));
      return new Signal(reinterpretedValues, unit);
    } else {
      const reinterpretedValues = this.values.flatMap(value => unit.digitsOfBE(value, {count: unit.digitsPer(this.unit)}));
      return new Signal(reinterpretedValues, unit);
    }
  }
}

export function bits(values: number[] | string): Signal {
  const numbers = typeof values === 'string'
    ? [...values].map(v => parseInt(v, 2))
    : values;
  return new Signal(numbers, BIT);
}

export function nibbles(values: number[] | string): Signal {
  const numbers = typeof values === 'string'
    ? [...values].map(v => parseInt(v, 16))
    : values;
  return new Signal(numbers, NIBBLE);
}

export function bytes(values: number[] | string): Signal {
  return typeof values === 'string'
    ? nibbles(values).as(BYTE)
    : new Signal(values, BYTE);
}

export function words(values: number[] | string): Signal {
  return typeof values === 'string'
    ? nibbles(values).as(WORD)
    : new Signal(values, WORD);
}

function chunks<T>(value: T[], valuesPerChunk: number) {
  if (value.length % valuesPerChunk !== 0 || valuesPerChunk == 0) {
    throw new Error(`Array of length ${value.length} cannot be split in chunks of size ${valuesPerChunk}`);
  }
  return Array(value.length / valuesPerChunk)
    .fill(undefined)
    .map((v, i) => value.slice(i * valuesPerChunk, (i + 1) * valuesPerChunk));
}

function valueOf(digits: number[], bitsPerDigit: number) {
  return digits.reduce((result, value) => (result << bitsPerDigit) + value, 0);
}

function assertSameShape(signals: Signal[]) {
  assertSameUnit(signals);
  assertSameLength(signals);
}

function assertSameUnit(signals: Signal[]) {
  signals.reduce((previous, current) => {
    if (current.unit !== previous.unit) {
      throw new Error('The provided signals have different units');
    }
    return current;
  });
}

function assertSameLength(signals: Signal[]) {
  signals.reduce((previous, current) => {
    if (current.length !== previous.length) {
      throw new Error('The provided signals have different lengths');
    }
    return current;
  });
}
