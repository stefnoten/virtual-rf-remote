import {BYTE} from './Unit';

describe('digitsOfBE', () => {
  test('returns big end first', () => {
    const result = BYTE.digitsOfBE(0x1234, {count: 2});
    expect(result).toEqual([0x12, 0x34]);
  });
});

describe('digitsOfLE', () => {
  test('returns little end first', () => {
    const result = BYTE.digitsOfLE(0x1234, {count: 2});
    expect(result).toEqual([0x34, 0x12]);
  });
});
