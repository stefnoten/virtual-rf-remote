import {bits, bytes, nibbles, words} from './Signal';
import {BIT, BYTE, NIBBLE, WORD} from './Unit';

describe('bits', () => {
  test('from string', () => {
    const value = '0110';
    const result = bits(value);
    expect(result.toString()).toEqual(value);
    expect(result.length).toEqual(4);
  });

  test('from numbers', () => {
    expect(`${bits([0, 1, 1, 0])}`).toEqual('0110');
  });

  [[-1], [2], '2', 'a', 'g'].forEach(value => {
    test(`fails on invalid value: ${value}`, () => {
      expect(() => bits(value)).toThrow();
    });
  });

  test('conversions', () => {
    expect(bits('011').as(BIT)).toEqual(bits('011'));
    expect(bits('11011011').as(NIBBLE)).toEqual(nibbles('db'));
    expect(bits('0010000111011011').as(BYTE)).toEqual(bytes('21db'));
    expect(bits('00100100110000000010000111011011').as(WORD)).toEqual(words('24c021db'));
  });
});

describe('nibbles', () => {
  test('from string', () => {
    const value = '0123456789abcdefABCDEF';
    const result = nibbles(value);
    expect(result.toString()).toEqual('0123456789abcdefabcdef');
    expect(result.length).toEqual(22);
  });

  test('from numbers', () => {
    expect(`${nibbles([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15])}`).toEqual('0123456789abcdef');
  });

  [[-1], [16], 'g'].forEach(value => {
    test(`fails on invalid value: ${value}`, () => {
      expect(() => nibbles(value)).toThrow();
    });
  });

  test('conversions', () => {
    expect(nibbles('1db').as(BIT)).toEqual(bits('000111011011'));
    expect(nibbles('1db').as(NIBBLE)).toEqual(nibbles('1db'));
    expect(nibbles('21db').as(BYTE)).toEqual(bytes('21db'));
    expect(nibbles('24c021db').as(WORD)).toEqual(words('24c021db'));
  });
});

describe('bytes', () => {
  test('from string', () => {
    const value = '0123456789abcdefABCDEF';
    const result = bytes(value);
    expect(result.toString()).toEqual('01 23 45 67 89 ab cd ef ab cd ef');
    expect(result.length).toEqual(11);
  });

  test('from numbers', () => {
    expect(`${bytes([0, 15, 255])}`).toEqual('00 0f ff');
  });

  [[-1], [2 ** 8], 'g'].forEach(value => {
    test(`fails on invalid value: ${value}`, () => {
      expect(() => bytes(value)).toThrow();
    });
  });

  test('conversions', () => {
    expect(bytes('21db').as(BIT)).toEqual(bits('0010000111011011'));
    expect(bytes('21db').as(NIBBLE)).toEqual(nibbles('21db'));
    expect(bytes('21db').as(BYTE)).toEqual(bytes('21db'));
    expect(bytes('24c021db').as(WORD)).toEqual(words('24c021db'));
  });
});

describe('words', () => {
  test('from string', () => {
    const value = '0123456789abcdefABCDEFEF';
    const result = words(value);
    expect(result.toString()).toEqual('0123 4567 89ab cdef abcd efef');
    expect(result.length).toEqual(6);
  });

  test('from numbers', () => {
    expect(`${words([0, 15, 255, 4095, 65535])}`).toEqual('0000 000f 00ff 0fff ffff');
  });

  [[-1], [2 ** 16], 'g'].forEach(value => {
    test(`fails on invalid value: ${value}`, () => {
      expect(() => bytes(value)).toThrow();
    });
  });

  test('conversions', () => {
    const value = words('24c021db');
    expect(value.as(BIT)).toEqual(bits('00100100110000000010000111011011'));
    expect(value.as(NIBBLE)).toEqual(nibbles('24c021db'));
    expect(value.as(BYTE)).toEqual(bytes('24c021db'));
    expect(value.as(WORD)).toEqual(words('24c021db'));
  });
});
