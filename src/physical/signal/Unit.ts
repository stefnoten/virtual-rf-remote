export class Unit {
  readonly hexDigits = Math.ceil(this.bitsPerDigit / 4);
  private maxExclusive = 2 ** this.bitsPerDigit;

  constructor(readonly bitsPerDigit: number, private name: string) {
  }

  static smallest(units: Unit[]) {
    return units.reduce((a, b) => Unit.min(a, b));
  }

  static min(unit1: Unit, unit2: Unit) {
    return unit1 < unit2 ? unit1 : unit2;
  }

  leastSignificant(value: number) {
    return value & (this.maxExclusive - 1);
  }

  digitsOfBE(value: number, {count}: { count: number }) {
    return this.digitsOfLE(value, {count}).reverse();
  }

  digitsOfLE(value: number, {count}: { count: number }) {
    if (value >= this.maxExclusive ** count) {
      throw new Error(`Value '${value}' cannot be represented by ${count} ${this.name}s`);
    }
    return Array.from(Array(count), (v, i) => this.leastSignificant(value >> i * this.bitsPerDigit));
  }

  digitsPer(other: Unit) {
    return Math.ceil(other.bitsPerDigit / this.bitsPerDigit);
  }

  toHex(value: number) {
    this.validate(value);
    return value.toString(16).padStart(this.hexDigits, '0');
  }

  validate(value: number) {
    if (isNaN(value) || value < 0 || value >= this.maxExclusive) {
      throw new Error(`Value '${value}' is not a valid ${this.name}`);
    }
  }

  valueOf() {
    return this.bitsPerDigit;
  }

  toString() {
    return this.name;
  }
}

export const BIT = new Unit(1, 'bit');
export const NIBBLE = new Unit(4, 'nibble');
export const BYTE = new Unit(8, 'byte');
export const WORD = new Unit(16, 'word');
