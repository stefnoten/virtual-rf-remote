import pigpio, {pi} from '@node-pigpio/core';
import Graceful from 'node-graceful';

export interface GpioConfig {
  host: string;
  port?: number;
}

export type Gpio = Omit<pigpio, 'connected' | 'stop'>;

class Connection {
  private instance: Promise<pigpio> | null = null;

  constructor(private config: GpioConfig) {
  }

  async get() {
    if (this.instance === null) {
      try {
        this.instance = pi(this.config.host, this.config.port);
        await this.instance;
        console.log('Connected to pigpiod');
      } catch (e) {
        console.error('Could not connect to pigpiod');
        throw e;
      }
    }
    return this.instance;
  }

  async end() {
    if (this.instance !== null) {
      const instance = await this.instance;
      console.log('Disconnecting from pigpiod...');
      await instance.stop();
    }
  }
}

export function loadGpio(config: GpioConfig): Gpio {
  const connection = new Connection(config);
  Graceful.on('exit', () => connection.end());
  return new Proxy({} as Gpio, {
    get(target, property, receiver) {
      return async function () {
        return Reflect.get(await connection.get(), property, receiver)(...arguments);
      };
    }
  });
}
