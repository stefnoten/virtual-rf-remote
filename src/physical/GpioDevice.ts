import {Cleanup} from '../utils';
import {Wave} from './wave';

export interface GpioDeviceConfig {
  gpio_pin: number;
}

export interface GpioDevice {
  send(wave: Wave): Promise<void>;

  sendRepeated(wave: Wave): Cleanup;
}
