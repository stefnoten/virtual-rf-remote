export type Cleanup = () => void;

export function composeCleanup(...cleanups: Cleanup[]) {
  return () => cleanups.forEach(c => c());
}
