import {sleep} from './sleep';

const MAX_INTERVAL = 1000;

export async function until(condition: () => Promise<boolean> | boolean) {
  let interval = 10;
  while (!await condition()) {
    await sleep(interval);
    interval = Math.max(interval * 2, MAX_INTERVAL);
  }
}
