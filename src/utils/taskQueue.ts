import {noop} from './noop';

export function taskQueue() {
  let done = Promise.resolve();
  return {
    async next<R>(task: () => PromiseLike<R>) {
      const result = done.then(() => task());
      done = result.then(noop, noop);
      return result;
    }
  };
}
