import {deferred} from './deferred';
import {taskQueue} from './taskQueue';

test('runs task', async () => {
  // given
  const queue = taskQueue();
  const expectedResult = 'ok';
  let called = false;

  // when
  const result = queue.next(async () => {
    called = true;
    return expectedResult;
  });
  await tick();

  // then
  expect(called).toBe(true);
  expect(await result).toBe(expectedResult);
});

test('does not start subsequent tasks before the previous ones are finished', async () => {
  // given
  const queue = taskQueue();
  const task1 = deferred<string>();
  const task2 = deferred<string>();
  const task3 = deferred<string>();
  let called2 = false;
  let called3 = false;

  // when
  const result1 = queue.next(() => task1);
  const result2 = queue.next(() => {
    called2 = true;
    return task2;
  });
  const result3 = queue.next(() => {
    called3 = true;
    return task3;
  });
  await tick();

  // then
  expect(called2).toBe(false);
  expect(called3).toBe(false);

  task1.resolve('result1');
  expect(await result1).toBe('result1');
  expect(called2).toBe(true);
  expect(called3).toBe(false);

  task2.resolve('result2');
  expect(await result2).toBe('result2');
  expect(called3).toBe(true);

  task3.resolve('result3');
  expect(await result3).toBe('result3');
});

test('executes the next task when the previous one fails', async () => {
  // given
  const queue = taskQueue();
  const task1 = deferred<string>();
  const task2 = deferred<string>();
  let called2 = false;

  // when
  const result1 = queue.next(() => task1);
  const result2 = queue.next(() => {
    called2 = true;
    return task2;
  });
  await tick();

  // then
  expect(called2).toBe(false);

  task1.reject('error1');
  await expect(result1).rejects.toBe('error1');
  expect(called2).toBe(true);

  task2.resolve('result2');
  expect(await result2).toBe('result2');
});

function tick() {
  return new Promise<void>(resolve => queueMicrotask(() => resolve()));
}
