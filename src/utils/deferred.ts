import {noop} from './noop';

export type Deferred<T> = PromiseLike<T> & {
  resolve: (result: T) => void;
  reject: (error: any) => void;
};

export function deferred<T>(): Deferred<T> {
  let delegate: Promise<T>;
  const result: Deferred<T> = {
    resolve: noop,
    reject: noop,
    then: (onfulfilled?, onrejected?) => delegate.then(onfulfilled, onrejected)
  };
  delegate = new Promise<T>((resolve, reject) => {
    result.resolve = resolve;
    result.reject = reject;
  });
  return result;
}
