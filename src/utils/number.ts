export function isEven(value: number) {
  return (value & 1) === 0;
}

export function isOdd(value: number) {
  return (value & 1) === 1;
}
