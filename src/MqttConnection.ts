import mqtt, {MqttClient} from 'mqtt';

export interface MqttConfig {
  base_topic: string;
  server: string;
  user: string;
  password: string;
  keepalive: number;
}

export enum Availability {
  ONLINE = 'online',
  OFFLINE = 'offline',
}

export type MqttTopic =
  | { topic: string }
  | { childTopic: string; baseTopic?: string }

export type MqttMessage<T extends object | string = any> = MqttTopic & {
  payload: T;
  retain?: boolean;
  qos?: 0 | 1 | 2;
};

export class MqttConnection {
  readonly baseTopic = this.config.base_topic;
  readonly availabilityTopic = this.topic({childTopic: 'availability'});
  readonly discoveryTopic = 'homeassistant';
  private client: MqttClient;

  constructor(private config: MqttConfig) {
  }

  async connect() {
    const {server, user, password, keepalive} = this.config;
    this.client = mqtt.connect(server, {
      username: user,
      password,
      keepalive,
      will: {
        topic: this.availabilityTopic,
        payload: Availability.OFFLINE,
        retain: true,
        qos: 1
      }
    });
    await new Promise<void>((resolve, reject) => {
      this.client.once('connect', () => resolve());
      this.client.once('error', err => {
        this.client.end();
        reject(err);
      });
    });
    await this.publish({topic: this.availabilityTopic, payload: Availability.ONLINE});
  }

  async publishAll(messages: MqttMessage[]) {
    await Promise.allSettled(messages.map(m => this.publish(m)));
  }

  async publish(message: MqttMessage) {
    const {payload, retain = true, qos = 1} = message;
    const serializedPayload = typeof payload === 'object' ? JSON.stringify(payload) : payload;
    return new Promise<void>((resolve, reject) => this.client.publish(
      this.topic(message),
      serializedPayload,
      {retain, qos},
      error => error ? reject(error) : resolve()
    ));
  }

  topic(topic: MqttTopic) {
    return 'topic' in topic
      ? topic.topic
      : `${topic.baseTopic ?? this.baseTopic}/${topic.childTopic}`;
  }

  async subscribe(topic: MqttTopic) {
    return new Promise<void>((resolve, reject) => {
      this.client.subscribe(this.topic(topic), error => {
        if (error) {
          reject(error);
        } else {
          resolve();
        }
      });
    });
  }

  onMessage(handler: (topicSegments: string[], payload: Buffer) => void) {
    this.client.on('message', (topic, payload) => {
      const [expectedBaseTopic, ...segments] = topic.split('/');
      if (expectedBaseTopic === this.baseTopic) {
        handler(segments, payload);
      }
    });
  }
}
